package com.islam.populatpeople.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.islam.populatpeople.R;
import com.islam.populatpeople.databinding.AdapterPersonsBinding;
import com.islam.populatpeople.interfaces.SelectItem;
import com.islam.populatpeople.model.models.Result;
import com.islam.populatpeople.utilities.Const;

import java.util.List;

public class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.ViewHolder> {
    AdapterPersonsBinding binding;
    Context context;
    List<Result> results;
    SelectItem item;
    public PersonsAdapter(Context context, List<Result> results, SelectItem item) {
        this.context=context;
        this.results=results;
        this.item=item;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        binding= DataBindingUtil.inflate(layoutInflater, R.layout.adapter_persons,parent,false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.binding.setResult(results.get(position));
        Glide.with(context).load(Const.IMAGEPATH+results.get(position).getProfilePath()).into(holder.binding.img);
        binding.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               item.Item(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return results.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        AdapterPersonsBinding binding;

        public ViewHolder(@NonNull     AdapterPersonsBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }

}
