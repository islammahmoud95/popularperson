package com.islam.populatpeople.network;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;


import com.islam.populatpeople.interfaces.GetMainResponse;
import com.islam.populatpeople.interfaces.ResponseStatues;
import com.islam.populatpeople.model.MainResponse;
import com.islam.populatpeople.utilities.Const;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repositrios {

    MutableLiveData<MainResponse> popularList=new MutableLiveData<>();
    MutableLiveData<MainResponse> person=new MutableLiveData<>();
    MutableLiveData<MainResponse> images=new MutableLiveData<>();


    public MutableLiveData<MainResponse> PopularPerson(int page, final ResponseStatues responseStatues,final GetMainResponse mainResponse){

        ApiEndPoint apiInterface = APIClient.getClient().create(ApiEndPoint.class);
        Call<MainResponse> JoinUs = apiInterface.Popular(
                Const.KEY,"en-US",page
        );
        JoinUs.enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, Response<MainResponse> response) {
                if (response.isSuccessful())
                {
                    responseStatues.ResponseSuccess();
                    popularList.setValue(response.body());
                    mainResponse.ReturnMainResponse(response);
                }
                else   responseStatues.ResponseFailed("Server Error");
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {
                responseStatues.ResponseFailed(t.getMessage());
            }
        });


        return popularList;
    }

    public MutableLiveData<MainResponse> Details(int id, final ResponseStatues responseStatues,final GetMainResponse mainResponse){

        ApiEndPoint apiInterface = APIClient.getClient().create(ApiEndPoint.class);
        Call<MainResponse> JoinUs = apiInterface.Person(
                id,  Const.KEY,"en-US"
        );
        JoinUs.enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, Response<MainResponse> response) {
                if (response.isSuccessful())
                {
                    responseStatues.ResponseSuccess();
                    person.setValue(response.body());
                    mainResponse.ReturnMainResponse(response);
                }
                else   responseStatues.ResponseFailed("Server Error");
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {
                responseStatues.ResponseFailed(t.getMessage());
            }
        });


        return person;
    }


    public MutableLiveData<MainResponse> Images(int id, final ResponseStatues responseStatues,final GetMainResponse mainResponse){

        ApiEndPoint apiInterface = APIClient.getClient().create(ApiEndPoint.class);
        Call<MainResponse> JoinUs = apiInterface.Images(
                id,Const.KEY
        );
        JoinUs.enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, Response<MainResponse> response) {
                if (response.isSuccessful())
                {
                    responseStatues.ResponseSuccess();
                    images.setValue(response.body());
                    mainResponse.ReturnMainResponse(response);
                }
                else   responseStatues.ResponseFailed("Server Error");
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {
                responseStatues.ResponseFailed(t.getMessage());
            }
        });


        return images;
    }

}
