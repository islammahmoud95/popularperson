package com.islam.populatpeople.network;

import android.content.Context;

import com.islam.populatpeople.utilities.Const;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    public static Context mcontext;
    private static int REQUEST_TIMEOUT = 60;
    private static OkHttpClient okHttpClient;
    private static Retrofit retrofit = null;

    public APIClient(Context context) {
        mcontext = context.getApplicationContext();
    }


    public static Retrofit getClient() {

        if (okHttpClient == null)
            initOkHttp();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Const.BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    private static void initOkHttp() {
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        httpClient.addInterceptor(interceptor);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder();
//                        .addHeader("Accept", "application/json")
//                        .addHeader("Content-Type", "application/json");

                HttpUrl url = original.url()
                        .newBuilder()
//                        .addQueryParameter(QUERY_PARAM_EMAIL_NAME, QUERY_PARAM_EMAIL_VALUE)
//                    .addQueryParameter(QUERY_PARAM_PASSWORD_NAME,QUERY_PARAM_PASSWORD_VALUE)
                        .build();

                Request request = requestBuilder.url(url).build();
                return chain.proceed(request);
            }
        });

        okHttpClient = httpClient.build();
    }
}
