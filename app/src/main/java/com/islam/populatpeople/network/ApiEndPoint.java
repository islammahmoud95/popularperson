package com.islam.populatpeople.network;






import com.islam.populatpeople.model.MainResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiEndPoint {


    @GET("person/popular")
    Call<MainResponse> Popular(
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") int page

    );



    @GET("person/{person_id}")
    Call<MainResponse> Person(
            @Path("person_id") int id,
            @Query("api_key") String api_key,
            @Query("language") String language
    );



    @GET("person/{person_id}/images")
    Call<MainResponse> Images(
            @Path("person_id") int id,
            @Query("api_key") String api_key);







}
