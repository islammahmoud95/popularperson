package com.islam.populatpeople.utilities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.LocaleList;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.islam.populatpeople.ui.SplashActivity;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {




    Bitmap bitmap;
    String image_str;
    Context context;

    public Utilities(Context context) {
        this.context = context;
    }

    public static boolean HasConnection(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public static String CreateDate(String dateStr, int type){
        SimpleDateFormat simpleDateFormat,hourFormat,dateFormat;
        simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat=new SimpleDateFormat("dd-M-yyyy");
        hourFormat=new SimpleDateFormat("k:mm");


        String datereturned="";

        Date date= null;
        try {
            date = simpleDateFormat.parse(dateStr);
            if (type==1)
            datereturned=dateFormat.format(date) +" " + hourFormat.format(date);
            else if (type==2)
                datereturned=dateFormat.format(date) +"\n" + hourFormat.format(date);

            Log.e("ffffffffffffx",datereturned);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ffffffffffff",e.getMessage());
        }

        return datereturned;
    }



}
