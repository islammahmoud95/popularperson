package com.islam.populatpeople.ui;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.material.snackbar.Snackbar;
import com.islam.populatpeople.MainActivity;
import com.islam.populatpeople.databinding.PopularPeopleFragmentBinding;
import com.islam.populatpeople.interfaces.GetMainResponse;
import com.islam.populatpeople.interfaces.ResponseStatues;
import com.islam.populatpeople.model.MainResponse;
import com.islam.populatpeople.network.Repositrios;
import com.islam.populatpeople.utilities.Utilities;

import retrofit2.Response;

public class PopularPeopleViewModel extends ViewModel implements ResponseStatues, GetMainResponse {
    Repositrios repos;
    String lang;
    MutableLiveData<MainResponse> mainResponse;
    Context context;
    int page;

    PopularPeopleFragmentBinding binding;
    public void getInit(Context context, PopularPeopleFragmentBinding binding){
        this.context=context;
        this.binding=binding;
        repos=new Repositrios();
        mainResponse=new MutableLiveData<>();
        binding.progressBar.setVisibility(View.VISIBLE);
        if (!Utilities.HasConnection(context)){
            Snackbar.make(binding.getRoot(), "No Internet Connection please check your connection", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Retry", new MyUndoListener()).show();
        }
        else {
            page=1;
            LoadData(page);
        }
    }

    public void LoadData(int page)
    {
        this.page=page;
        repos.PopularPerson(page,this,this);

    }

    @Override
    public void ResponseSuccess() {
        binding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void ResponseFailed(String message) {
        binding.progressBar.setVisibility(View.GONE);
        Snackbar.make(binding.getRoot(), "No Internet Connection please check your connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new MyUndoListener()).show();
    }

    public LiveData<MainResponse> getMainResponse() {
        return mainResponse;
    }

    @Override
    public void ReturnMainResponse(Response mainResponse) {
        binding.progressBar.setVisibility(View.GONE);
        this.mainResponse.setValue((MainResponse) mainResponse.body());
    }


    public class MyUndoListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
                LoadData(page);
        }
    }
}