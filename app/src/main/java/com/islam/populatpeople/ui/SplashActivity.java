package com.islam.populatpeople.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.islam.populatpeople.MainActivity;
import com.islam.populatpeople.R;
import com.islam.populatpeople.databinding.ActivitySplashBinding;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {


    ActivitySplashBinding binding;
    public int progress=0;
    Handler handler=new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        binding= DataBindingUtil.setContentView(this, R.layout.activity_splash);
        binding.setSplash(this);
        CreatCounter();

    }
    public void CreatCounter(){


        CountDownTimer countDownTimer=new CountDownTimer(2000,1000) {
            @Override
            public void onTick(long l) {


            }

            @Override
            public void onFinish() {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

            }
        }.start();
    }


}
