package com.islam.populatpeople.ui;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.islam.populatpeople.MainActivity;
import com.islam.populatpeople.R;
import com.islam.populatpeople.adapter.GalleryAdapter;
import com.islam.populatpeople.adapter.PersonsAdapter;
import com.islam.populatpeople.databinding.DetailesFragmentBinding;
import com.islam.populatpeople.interfaces.SelectItem;
import com.islam.populatpeople.model.MainResponse;
import com.islam.populatpeople.model.models.Result;
import com.islam.populatpeople.utilities.Const;

import java.util.ArrayList;
import java.util.List;

public class DetailesFragment extends Fragment implements SelectItem {

    private DetailesViewModel mViewModel;
    int  id;
    static String ARGS="ARGS";
    static String ARGS1="ARGS1";
    private FragmentActivity activity;
    private DetailesFragmentBinding binding;
    private String title;
    private GalleryAdapter galleryAdapter;
    List<Result> results;

    public static DetailesFragment newInstance(int id,String title) {
        DetailesFragment fragment=new DetailesFragment();
        Bundle bundle=new Bundle();
        bundle.putInt(ARGS,id);
        bundle.putString(ARGS1,title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null) {
            id = getArguments().getInt(ARGS);
            title = getArguments().getString(ARGS1);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (getActivity()!=null)
            activity=getActivity();
        ((MainActivity) activity ).setTitle(title);

        mViewModel=new ViewModelProvider(activity).get(DetailesViewModel.class);
        binding= DataBindingUtil.inflate(inflater,R.layout.detailes_fragment, container, false);
        mViewModel.getInit(activity,binding,id);
        binding.setDetails(mViewModel);
        binding.galleryList.setHasFixedSize(true);
        binding.galleryList.setLayoutManager(new GridLayoutManager(activity,2));
        results=new ArrayList<>();
        galleryAdapter=new GalleryAdapter(activity,results,this);
        binding.galleryList.setAdapter(galleryAdapter);
        mViewModel.getDetailsResponse().observe(activity, new Observer<MainResponse>() {
            @Override
            public void onChanged(MainResponse mainResponse) {
                if (mainResponse!=null)
                {
                    MainResponse mainResponse1=mainResponse;
                    binding.setData(mainResponse);
                    Glide.with(activity).load(Const.IMAGEPATH+mainResponse.getProfilePath()).into(binding.avatar);
                }
            }
        });
        mViewModel.getImagesResponse().observe(activity, new Observer<MainResponse>() {
            @Override
            public void onChanged(MainResponse mainResponse) {
                if (mainResponse!=null)
                {
                    if (mainResponse.getProfiles()!=null)
                    {
                        results.clear();
                        galleryAdapter.notifyDataSetChanged();
                        results.addAll(mainResponse.getProfiles());
                        galleryAdapter.notifyDataSetChanged();
                    }
                }
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DetailesViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void Item(int position) {
        Intent intent=new Intent(activity,ImageViewerActivity.class);
        intent.putExtra("img",Const.IMAGEPATH+results.get(position).getFilePath());
        startActivity(intent);
    }





}