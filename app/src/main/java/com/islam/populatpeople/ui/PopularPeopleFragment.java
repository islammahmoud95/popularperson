package com.islam.populatpeople.ui;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;
import com.islam.populatpeople.MainActivity;
import com.islam.populatpeople.R;
import com.islam.populatpeople.adapter.PersonsAdapter;
import com.islam.populatpeople.databinding.PopularPeopleFragmentBinding;
import com.islam.populatpeople.interfaces.SelectItem;
import com.islam.populatpeople.model.MainResponse;
import com.islam.populatpeople.model.models.Result;
import com.islam.populatpeople.utilities.CustomScrollListener;
import com.islam.populatpeople.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

public class PopularPeopleFragment extends Fragment implements SelectItem {

    private PopularPeopleViewModel mViewModel;
    private FragmentActivity activity;
    private PopularPeopleFragmentBinding binding;
    List<Result> results;
    String title;
    int page=1;
    int totalPage;
    boolean loadingData=true;
    PersonsAdapter personsAdapter;
    public static PopularPeopleFragment newInstance() {
        return new PopularPeopleFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (getActivity()!=null)
            activity=getActivity();
        ((MainActivity) activity ).setTitle(getString(R.string.app_name));

        mViewModel=new ViewModelProvider(activity).get(PopularPeopleViewModel.class);
        binding= DataBindingUtil.inflate(inflater,R.layout.popular_people_fragment, container, false);
        mViewModel.getInit(activity,binding);
        binding.setPersons(mViewModel);
        results=new ArrayList<>();
        binding.list.setHasFixedSize(true);
        binding.list.setLayoutManager(new GridLayoutManager(activity,2));
        personsAdapter=new PersonsAdapter(activity,results,this);
        binding.list.setAdapter(personsAdapter);
        mViewModel.getMainResponse().observe(activity, new Observer<MainResponse>() {
            @Override
            public void onChanged(MainResponse mainResponse) {
                if (mainResponse!=null)
                {
                    if (mainResponse.getResults()!=null)
                    {
                        Log.e("loadngPage",page+"");
                        page=mainResponse.getPage();
                        totalPage=mainResponse.getTotalPages();
                        loadingData=false;
                        List<Result> resultss = new ArrayList<>(mainResponse.getResults());
                        results.addAll(resultss);
                        personsAdapter.notifyDataSetChanged();
                    }
                }
            }
        });


        binding.list.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState==RecyclerView.SCROLL_STATE_SETTLING)
                {
                    if (!loadingData) {
                        if (page < totalPage) {
                            page=page+1;
                             mViewModel.LoadData(page);
                            loadingData=true;
                            binding.progressBar.setVisibility(View.VISIBLE);

                        }
                    }
                }
            }
        });


        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }

    @Override
    public void Item(int position) {
        getParentFragmentManager().beginTransaction().replace(R.id.container,DetailesFragment.newInstance(results.get(position).getId(),results.get(position).getName())).addToBackStack("").commit();
    }



}