package com.islam.populatpeople.ui;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.material.snackbar.Snackbar;
import com.islam.populatpeople.databinding.DetailesFragmentBinding;
import com.islam.populatpeople.databinding.PopularPeopleFragmentBinding;
import com.islam.populatpeople.interfaces.GetMainResponse;
import com.islam.populatpeople.interfaces.ResponseStatues;
import com.islam.populatpeople.model.MainResponse;
import com.islam.populatpeople.network.Repositrios;
import com.islam.populatpeople.utilities.Utilities;

import retrofit2.Response;

public class DetailesViewModel extends ViewModel implements ResponseStatues, GetMainResponse {
    Repositrios repos;
    String lang;
    MutableLiveData<MainResponse> imagesResponse;
    MutableLiveData<MainResponse> detailsResponse;
    Context context;
    int id;

    DetailesFragmentBinding binding;
    public void getInit(Context context, DetailesFragmentBinding binding,int id){
        this.context=context;
        this.binding=binding;
        repos=new Repositrios();
        imagesResponse=new MutableLiveData<>();
        detailsResponse=new MutableLiveData<>();
        this.id=id;
        binding.progressBar.setVisibility(View.VISIBLE);


            detailsResponse=(repos.Details(id,this,this));
            imagesResponse=(repos.Images(id,this,this));
    }

    public void LoadData()
    {
        detailsResponse=(repos.Details(id,this,this));
        imagesResponse=(repos.Images(id,this,this));

    }

    @Override
    public void ResponseSuccess() {
        Log.e("xxxxxxx","success");
        binding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void ResponseFailed(String message) {
        binding.progressBar.setVisibility(View.GONE);
        Snackbar.make(binding.getRoot(), "No Internet Connection please check your connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new MyUndoListener()).show();
    }



    @Override
    public void ReturnMainResponse(Response mainResponse) {
        binding.progressBar.setVisibility(View.GONE);

    }


    public class MyUndoListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            LoadData();
        }
    }

    public LiveData<MainResponse> getImagesResponse() {
        return imagesResponse;
    }

    public LiveData<MainResponse> getDetailsResponse() {
        return detailsResponse;
    }
}