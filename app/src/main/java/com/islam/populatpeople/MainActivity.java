package com.islam.populatpeople;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.islam.populatpeople.ui.PopularPeopleFragment;
import com.islam.populatpeople.utilities.PermissionUtils;
import com.islam.populatpeople.utilities.Utilities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private String [] storagePermission = {Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CreatePermission();

        getSupportFragmentManager().beginTransaction().replace(R.id.container, PopularPeopleFragment.newInstance()).commit();


    }

    public boolean CreatePermission()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED )
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (PermissionUtils.neverAskAgainSelected(this,Manifest.permission.READ_EXTERNAL_STORAGE))
                {
                    Log.e("MainFrag", "Denied.");

                    PermissionUtils.displayNeverAskAgainDialog(this,this.getResources().getString(R.string.readWriteStorage));
                } else if (PermissionUtils.neverAskAgainSelected(this,Manifest.permission.WRITE_EXTERNAL_STORAGE))
                {
                    Log.e("MainFrag", "Denied.");
                    PermissionUtils.displayNeverAskAgainDialog(this,this.getResources().getString(R.string.readWriteStorage));
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                        storagePermission,
                        15);
            }
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e("xxxx","Req");
        switch (requestCode) {
            case 15:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.

                }  else {
                    PermissionUtils.setShouldShowStatus(this, Manifest.permission.READ_EXTERNAL_STORAGE);
                    PermissionUtils.setShouldShowStatus(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    return;
                }

        }
    }

}